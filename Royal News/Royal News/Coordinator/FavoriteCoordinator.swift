//
//  FavoriteCoordinator.swift
//  Royal News
//
//  Created by Catalina on 7/30/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
class FavoriteCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: RNNavigationController
    init(navigationController: RNNavigationController) {
        self.navigationController = navigationController
    }
    func start() {
        let tabbarItem = UITabBarItem(title: "Favorite", image: UIImage(systemName: "heart"), tag: 1)
        tabbarItem.selectedImage = UIImage(systemName: "heart.fill")
        let vc = FavoriteViewController()
        vc.tabBarItem = tabbarItem
        vc.tabBarItem.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    func toWebViewPage(_ viewController: UIViewController, indexPath: IndexPath, item: News) {
        let vc = NewsWebViewController()
        vc.favoriteCoordinator = self
        vc.delegate = (viewController as! NewsWebViewControllerDelegate)
        vc.indexPath = indexPath
        vc.item = item
        navigationController.pushViewController(vc, animated: true)
    }
}
