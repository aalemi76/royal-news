//
//  FeedCoordinator.swift
//  Royal News
//
//  Created by Catalina on 7/30/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
class FollowingCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: RNNavigationController
    init(navigationController: RNNavigationController) {
        self.navigationController = navigationController
    }
    func start() {
        let tabbarItem = UITabBarItem(title: "Following", image: UIImage(systemName: "square.stack"), tag: 0)
        tabbarItem.selectedImage = UIImage(named: "square.stack.fill")
        let vc = FollowingViewController()
        vc.tabBarItem = tabbarItem
        vc.tabBarItem.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    func toNYNewsPage() {
        let vc = NYNewsViewController()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    func toMultipleNewsPage() {
        let vc = MultipleNewsViewController()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    func toWebViewPage(_ viewController: UIViewController, indexPath: IndexPath, item: News) {
        let vc = NewsWebViewController()
        vc.coordinator = self
        vc.delegate = (viewController as! NewsWebViewControllerDelegate)
        vc.indexPath = indexPath
        vc.item = item
        navigationController.pushViewController(vc, animated: true)
    }
}
