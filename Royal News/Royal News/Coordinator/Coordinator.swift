//
//  Coordinator.swift
//  Royal News
//
//  Created by Catalina on 7/30/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
protocol Coordinator: AnyObject {
  var childCoordinators: [Coordinator] { get set }
  var navigationController: RNNavigationController { get set }
  func start()
}
