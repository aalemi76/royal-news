//
//  XMLManager.swift
//  Royal News
//
//  Created by Catalina on 7/31/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
import Alamofire
class XMLManager: NSObject {
    private var feedItems = [News]()
    private var element = ""
    private var title = "" {
        didSet {
            title = title.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    private var itemDescription = "" {
        didSet {
            itemDescription = itemDescription.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    private var pubDate = "" {
        didSet {
            pubDate = pubDate.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    private var link = "" {
        didSet {
            link = link.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    private var imageURL = "" {
        didSet {
            imageURL = imageURL.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    private var src = "" {
        didSet {
            src = src.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    private var parserCompletionHandler: ((Result<[News], RNError>) -> Void)?
    func parse(url: String, completionHandler: @escaping ((Result<[News], RNError>) -> Void)) {
        parserCompletionHandler = completionHandler
        AF.request(url, method: .get).response { (data) in
            if let _ = data.error {
                completionHandler(.failure(.unableToComplete))
            }
            guard let data = data.data else {
                completionHandler(.failure(.invalidData))
                return
            }
            let parser = XMLParser(data: data)
            parser.delegate = self
            parser.parse()
        }
    }
    func resetNewsVariables() {
        title = ""
        itemDescription = ""
        pubDate = ""
        link = ""
        imageURL = ""
        src = ""
    }
    func setNewsVariables(string: String) {
        switch element {
        case "title":
            title += string
        case "description":
            itemDescription += string
        case "pubDate":
            pubDate += string
        case "link":
            link += string
        default:
            break
        }
    }
}
extension XMLManager: XMLParserDelegate {
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        element = elementName
        if element == "item" {
            resetNewsVariables()
        } else if element == "media:content" {
            guard let url = attributeDict["url"] else {return}
            imageURL = url
        }
    }
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        guard element != "Channel" else {return}
        setNewsVariables(string: string)
    }
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "item" {
            let news = News(title: title, description: itemDescription, pubDate: pubDate, link: link, imageURL: imageURL, src: "The New York Times", isJson: false)
            feedItems.append(news)
        }
    }
    func parserDidEndDocument(_ parser: XMLParser) {
        parserCompletionHandler?(.success(feedItems))
    }
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        parserCompletionHandler?(.failure(.unableToComplete))
    }
}
