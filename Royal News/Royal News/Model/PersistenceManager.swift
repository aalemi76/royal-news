//
//  PersistenceManager.swift
//  Royal News
//
//  Created by Catalina on 7/31/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
struct PersistenceManager {
    static var favoriteURL: URL = {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0].appendingPathComponent("favorite.archive")
    }()
    static var items = [News]()
    static func saveItems() {
        do {
            let codedData = try NSKeyedArchiver.archivedData(withRootObject: PersistenceManager.items, requiringSecureCoding: false)
            try codedData.write(to: PersistenceManager.favoriteURL)
        } catch {
            print("Error saving notes: \(error.localizedDescription)")
        }
    }
    @discardableResult static func loadItems() -> [News]? {
        do {
            let codedData = try Data(contentsOf: PersistenceManager.favoriteURL)
            guard let array = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(codedData) as? [News] else { return nil }
            items = array
            print(array)
            return array
        } catch {
            print("Error loading notes: \(error.localizedDescription)")
            return nil
        }
    }
    static func configureFavorite(array: [News]) {
        for element in array {
            for (i, item) in items.enumerated() {
                if element == item {
                    element.isFavorite = true
                    items[i] = element
                }
            }
        }
    }
}
