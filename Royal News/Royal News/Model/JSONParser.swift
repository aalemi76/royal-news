//
//  JSONParser.swift
//  Royal News
//
//  Created by Catalina on 7/31/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
class JSONParser {
    func initializeArray(data: Data) -> [News]? {
        var result = [News]()
        let jsonObject = JSON(data)
        guard let articles = jsonObject["articles"].arrayObject as? [[String: Any]], !articles.isEmpty else {return nil}
        for article in articles {
            if let news = initializeNews(data: article) {
                result.append(news)
            }
        }
        return result
    }
    private func initializeNews(data: [String: Any]) -> News? {
        guard let title = data["title"] as? String, let description = data["description"] as? String, let pubDate = data["publishedAt"] as? String, let link = data["url"] as? String, let imageURL = data["urlToImage"] as? String, let source = data["source"] as? [String: String], let src = source["name"] else {
            return nil
        }
        return News(title: title, description: description, pubDate: pubDate, link: link, imageURL: imageURL, src: src, isJson: true)
    }
}
