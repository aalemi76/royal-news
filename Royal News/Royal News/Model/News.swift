//
//  News.swift
//  Royal News
//
//  Created by Catalina on 7/31/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
class News: NSObject, NSCoding {
    var title, itemDescription, pubDate, link, imageURL, src: String
    var date: Date?
    var isFavorite: Bool = false
    init(title: String, description: String, pubDate: String, link: String, imageURL:String, src: String, isJson: Bool) {
        self.title = title
        self.itemDescription = description
        self.pubDate = pubDate
        self.link = link
        self.imageURL = imageURL
        self.src = src
        super.init()
        setDate(isJson: isJson)
    }
    func setDate(isJson: Bool) {
        if isJson {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            date = dateFormatter.date(from: pubDate)
        } else {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
            date = dateFormatter.date(from: pubDate)
        }
    }
    func encode(with coder: NSCoder) {
        coder.encode(title, forKey: "title")
        coder.encode(itemDescription, forKey: "itemDescription")
        coder.encode(pubDate, forKey: "pubDate")
        coder.encode(link, forKey: "link")
        coder.encode(imageURL, forKey: "imageURL")
        coder.encode(src, forKey: "src")
        coder.encode(date, forKey: "date")
        coder.encode(isFavorite, forKey: "isFavorite")
    }
    required init?(coder: NSCoder) {
        title = coder.decodeObject(forKey: "title") as! String
        itemDescription = coder.decodeObject(forKey: "itemDescription") as! String
        pubDate = coder.decodeObject(forKey: "pubDate") as! String
        link = coder.decodeObject(forKey: "link") as! String
        imageURL = coder.decodeObject(forKey: "imageURL") as! String
        src = coder.decodeObject(forKey: "src") as! String
        date = coder.decodeObject(forKey: "date") as! Date?
        isFavorite = coder.decodeBool(forKey: "isFavorite")
        super.init()
    }
    static func == (lhs: News, rhs: News) -> Bool {
        if lhs.title == rhs.title && lhs.src == rhs.src {
            return true
        } else {
            return false
        }
    }
}
