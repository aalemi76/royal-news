//
//  RNNavigationController.swift
//  Royal News
//
//  Created by Catalina on 7/30/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
class RNNavigationController: UINavigationController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    let backIcon = UIImageView(image: UIImage(systemName: "arrow.left"))
    let padding: CGFloat = 20
    let itemWidth: CGFloat = 35
    let itemHeight: CGFloat = 35
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }
    func configureView() {
        let textAttributes = [NSAttributedString.Key.foregroundColor: GlobalSettings.shared().lightGray, NSAttributedString.Key.font: GlobalSettings.shared().boldSystemFont(size: 18)]
        navigationItem.setHidesBackButton(true, animated: false)
        navigationBar.titleTextAttributes = textAttributes
        navigationBar.barTintColor = GlobalSettings.shared().mainColor
    }
    func addBackIcon() {
        backIcon.isHidden = false
        backIcon.contentMode = .scaleAspectFit
        backIcon.tintColor = GlobalSettings.shared().lightGray
        backIcon.isUserInteractionEnabled = true
        backIcon.translatesAutoresizingMaskIntoConstraints = false
        navigationBar.addSubview(backIcon)
        NSLayoutConstraint.activate([
            backIcon.centerYAnchor.constraint(equalTo: navigationBar.centerYAnchor),
            backIcon.leadingAnchor.constraint(equalTo: navigationBar.leadingAnchor, constant: padding),
            backIcon.widthAnchor.constraint(equalToConstant: itemWidth),
            backIcon.heightAnchor.constraint(equalToConstant: itemHeight)])
    }
}
