//
//  Route.swift
//  Royal News
//
//  Created by Catalina on 7/31/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
struct Route {
    static var xmlURL: String {
        get {
            return "https://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml"
        }
    }
    static var jsonURL: String {
        get {
            return "https://newsapi.org/v2/everything"
        }
    }
    static var apiKey: String {
        get {
            return "e19a477472ec4a87953c13272cf48913"
        }
    }
    static var domain: String {
        get {
            return "bbc.co.uk, techcrunch.com, engadget.com"
        }
    }
    static var language: String {
        get {
            return "en"
        }
    }
    static func generateParameters() -> [String: String] {
        var params = [String: String]()
        params.updateValue(Route.apiKey, forKey: "apiKey")
        params.updateValue(Route.domain, forKey: "domains")
        params.updateValue(Route.language, forKey: "language")
        return params
    }
}
