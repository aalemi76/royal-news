//
//  RNTabBarController.swift
//  Royal News
//
//  Created by Catalina on 7/30/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
class RNTabBarController: UITabBarController {
    let favoriteCoordinator = FavoriteCoordinator(navigationController: RNNavigationController())
    let followingCoordinator = FollowingCoordinator(navigationController: RNNavigationController())
    override func viewDidLoad() {
        super.viewDidLoad()
        UITabBar.appearance().tintColor = GlobalSettings.shared().mainColor
        favoriteCoordinator.start()
        followingCoordinator.start()
        viewControllers = [followingCoordinator.navigationController,
                           favoriteCoordinator.navigationController]
    }
}
