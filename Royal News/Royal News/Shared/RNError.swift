//
//  RNError.swift
//  Royal News
//
//  Created by Catalina on 7/31/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
enum RNError: String, Error {
    case unableToSaveProfile = "There was an error adding this user. Please try again."
    case unableToRetriveProfile = "There was no profile to retrieve"
    case unableToDecodeProfile = "Unexpected type in profile"
    case invalidURL = "Somthing went wrong with domain"
    case unableToComplete = "Unable to complete your request. Please check your internet connection"
    case invalidResponse = "Invalid response from the server. Please try again."
    case invalidData = "The data received from the server was invalid. Please try again."
    case invalidJson = "Invalid response from the server. Please contact admin."
    case emptyFields = "Please fill the empty fields"
    case unableToDownloadImage = "There was an error while downloading image."
}
