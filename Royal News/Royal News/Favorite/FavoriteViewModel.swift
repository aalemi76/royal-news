//
//  FavoriteViewModel.swift
//  Royal News
//
//  Created by Catalina on 7/31/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
class FavoriteViewModel {
    let tableView = UITableView(frame: .zero, style: .plain)
    let emptyLabel = UILabel()
    init() {
        configureTableView()
        configureLabel()
    }
    func configureTableView() {
        tableView.register(NewsTableViewCell.self, forCellReuseIdentifier: NewsTableViewCell.reuseID)
        tableView.tableFooterView = UIView()
    }
    func configureLabel() {
        emptyLabel.text = "You have not favorited any news yet!"
        emptyLabel.font = GlobalSettings.shared().boldSystemFont(size: 17)
        emptyLabel.textColor = GlobalSettings.shared().darkGray
        emptyLabel.textAlignment = .center
    }
}

