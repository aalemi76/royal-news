//
//  FavoriteViewController.swift
//  Royal News
//
//  Created by Catalina on 7/30/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
class FavoriteViewController: UIViewController {
    weak var coordinator: FavoriteCoordinator?
    let model = FavoriteViewModel()
    var backIcon: UIImageView!
    var emptyLabel: UILabel!
    var tableView: UITableView!
    var items = PersistenceManager.items
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
        items = PersistenceManager.items
        items.sort { (item1, item2) -> Bool in
            guard let date1 = item1.date, let date2 = item2.date else {return true}
            return date1 > date2
        }
        configureViewState()
        tableView.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        addTableView()
        addEmptyLabel()
    }
    func configureNavigationBar() {
        guard let navController = navigationController as? RNNavigationController else {return}
        navController.setNavigationBarHidden(false, animated: true)
        navController.backIcon.isHidden = true
        navigationItem.title = "Favorite"
        navigationItem.hidesBackButton = true
    }
    func addTableView() {
        tableView = model.tableView
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 160
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)])
    }
    func addEmptyLabel() {
        emptyLabel = model.emptyLabel
        emptyLabel.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(emptyLabel)
        NSLayoutConstraint.activate([
            emptyLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            emptyLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)])
    }
    func configureViewState() {
        if items.count == 0 {
            emptyLabel.isHidden = false
            tableView.isHidden = true
        } else {
            emptyLabel.isHidden = true
            tableView.isHidden = false
        }
    }
}
