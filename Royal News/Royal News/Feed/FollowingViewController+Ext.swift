//
//  FollowingViewController+Ext.swift
//  Royal News
//
//  Created by Catalina on 7/30/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
extension FollowingViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 77
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ChannelTableViewCell.reuseID, for: indexPath) as? ChannelTableViewCell else {return UITableViewCell()}
        if indexPath.row == 0 {
            cell.confgiureCellItems(title: "The World News", image: UIImage(named: "news") ?? UIImage())
        } else {
            cell.confgiureCellItems(title: "The New York Times", image: UIImage(named: "NY") ?? UIImage())
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 0:
            coordinator?.toMultipleNewsPage()
        case 1:
            coordinator?.toNYNewsPage()
        default:
            return
        }
    }
}
