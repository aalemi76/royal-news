//
//  NewsWebViewController.swift
//  Royal News
//
//  Created by Catalina on 7/31/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
import WebKit
protocol NewsWebViewControllerDelegate: class {
    func backIconTouchUpInside(indexPath: IndexPath)
}
class NewsWebViewController: UIViewController {
    weak var coordinator: FollowingCoordinator?
    weak var favoriteCoordinator: FavoriteCoordinator?
    weak var delegate: NewsWebViewControllerDelegate?
    let model = NewsWebViewModel()
    var backIcon: UIImageView!
    var webView: WKWebView!
    var likeButton: UIButton!
    var item: News!
    var isFavorite: Bool = false {
        willSet {
            model.changeLikeButton(state: newValue)
        }
    }
    var indexPath: IndexPath!
    @objc func didTapBackIcon() {
        delegate?.backIconTouchUpInside(indexPath: indexPath)
        navigationController?.popViewController(animated: true)
    }
    @objc func didTapLikeButton() {
        if item.isFavorite {
            let index = PersistenceManager.items.firstIndex(of: item)
            item.isFavorite = false
            PersistenceManager.items.remove(at: index!)
        } else {
            item.isFavorite = true
            PersistenceManager.items.append(item)
        }
        isFavorite = item.isFavorite
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
        isFavorite = item.isFavorite
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        isFavorite = item.isFavorite
        addWebView()
        addLikeButton()
        loadWebPage()
    }
    func configureNavigationBar() {
        guard let navController = navigationController as? RNNavigationController else {return}
        navController.setNavigationBarHidden(false, animated: true)
        navController.addBackIcon()
        backIcon = navController.backIcon
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapBackIcon))
        backIcon.addGestureRecognizer(tapGestureRecognizer)
        navigationItem.title = "The News"
        navigationItem.hidesBackButton = true
    }
    func addWebView() {
        webView = model.webView
        webView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(webView)
        NSLayoutConstraint.activate([
            webView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            webView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            webView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            webView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)])
    }
    func addLikeButton() {
        likeButton = model.likeButton
        likeButton.isHidden = true
        likeButton.addTarget(self, action: #selector(didTapLikeButton), for: .touchUpInside)
        likeButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(likeButton)
        view.bringSubviewToFront(likeButton)
        NSLayoutConstraint.activate([
            likeButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -15),
            likeButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15),
            likeButton.widthAnchor.constraint(equalToConstant: 60),
            likeButton.heightAnchor.constraint(equalToConstant: 60)])
    }
    func loadWebPage() {
        guard let url = URL(string: item.link) else {return}
        likeButton.isHidden = false
        webView.load(URLRequest(url: url))
    }
}
