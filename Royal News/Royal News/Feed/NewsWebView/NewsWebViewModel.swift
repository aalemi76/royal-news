//
//  NewsWebViewModel.swift
//  Royal News
//
//  Created by Catalina on 7/31/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
import WebKit
class NewsWebViewModel {
    let webView = WKWebView()
    let likeButton: UIButton = {
        let button = UIButton(type: .system)
        button.tintColor = GlobalSettings.shared().mainColor
        return button
    }()
    func changeLikeButton(state: Bool) {
        if state {
            likeButton.setBackgroundImage(UIImage(systemName: "heart.slash.circle"), for: .normal)
        } else {
            likeButton.setBackgroundImage(UIImage(systemName: "heart.circle.fill"), for: .normal)
        }
    }
}
