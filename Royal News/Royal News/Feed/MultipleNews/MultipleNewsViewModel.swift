//
//  MultipleNewsViewModel.swift
//  Royal News
//
//  Created by Catalina on 7/31/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class MultipleNewsViewModel {
    let tableView = UITableView(frame: .zero, style: .plain)
    let manager = JSONParser()
    init() {
        configureTableView()
    }
    func configureTableView() {
        tableView.register(NewsTableViewCell.self, forCellReuseIdentifier: NewsTableViewCell.reuseID)
        tableView.tableFooterView = UIView()
    }
    func getNews(url: String, parameters: [String: String], completionHandler: @escaping ((Result<[News], RNError>) -> Void)) {
        AF.request(url, method: .get, parameters: parameters, encoding: URLEncoding.queryString).responseJSON { [weak self] response in
            guard let weakSelf = self, let data = response.data else {
                completionHandler(.failure(.invalidData))
                return
            }
            switch response.result {
            case .success:
                guard let items = weakSelf.manager.initializeArray(data: data) else {
                    completionHandler(.failure(.invalidJson))
                    return
                }
                completionHandler(.success(items))
            case .failure:
                completionHandler(.failure(.unableToComplete))
            }
        }
    }
}
