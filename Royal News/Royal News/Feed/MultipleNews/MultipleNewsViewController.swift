//
//  MultipleNewsViewController.swift
//  Royal News
//
//  Created by Catalina on 7/31/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
class MultipleNewsViewController: UIViewController {
    weak var coordinator: FollowingCoordinator?
    let model = MultipleNewsViewModel()
    var backIcon: UIImageView!
    var tableView: UITableView!
    var items = [News]()
    @objc func didTapBackIcon() {
        navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
        PersistenceManager.configureFavorite(array: items)
        tableView.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        addTableView()
        fetchData()
    }
    func fetchData() {
        let queue = DispatchQueue.global()
        queue.async { [weak self] in
            guard let weakSelf = self else {return}
            weakSelf.model.getNews(url: Route.jsonURL, parameters: Route.generateParameters()) { result in
                switch result {
                case .success(let data):
                    PersistenceManager.configureFavorite(array: data)
                    weakSelf.items = data
                    DispatchQueue.main.async {
                        weakSelf.tableView.reloadData()
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    func configureNavigationBar() {
        guard let navController = navigationController as? RNNavigationController else {return}
        navController.setNavigationBarHidden(false, animated: true)
        navController.addBackIcon()
        backIcon = navController.backIcon
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapBackIcon))
        backIcon.addGestureRecognizer(tapGestureRecognizer)
        navigationItem.title = "The World News"
        navigationItem.hidesBackButton = true
    }
    func addTableView() {
        tableView = model.tableView
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 160
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)])
    }
}
