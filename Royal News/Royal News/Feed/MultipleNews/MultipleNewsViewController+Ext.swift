//
//  MultipleNewsViewController+Ext.swift
//  Royal News
//
//  Created by Catalina on 7/31/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
extension MultipleNewsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let news = items[indexPath.row]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NewsTableViewCell.reuseID, for: indexPath) as? NewsTableViewCell else { return UITableViewCell()}
        cell.configure(forNews: news)
        cell.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let news = items[indexPath.row]
        coordinator?.toWebViewPage(self, indexPath: indexPath, item: news)
    }
}
extension MultipleNewsViewController: NewsTableViewCellDelegate {
    func didTapLikeButton(cell: NewsTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else {return}
        let item = items[indexPath.row]
        if item.isFavorite {
            let index = PersistenceManager.items.firstIndex(of: item)
            item.isFavorite = false
            PersistenceManager.items.remove(at: index!)
        } else {
            item.isFavorite = true
            PersistenceManager.items.append(item)
        }
    }
}
extension MultipleNewsViewController: NewsWebViewControllerDelegate {
    func backIconTouchUpInside(indexPath: IndexPath) {
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
}
