//
//  FollowingViewModel.swift
//  Royal News
//
//  Created by Catalina on 7/30/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
class FollowingViewModel {
    let tableView = UITableView(frame: .zero, style: .insetGrouped)
    init() {
        configureTableView()
    }
    func configureTableView() {
        tableView.register(ChannelTableViewCell.self, forCellReuseIdentifier: ChannelTableViewCell.reuseID)
        tableView.separatorStyle = .none
    }
}
