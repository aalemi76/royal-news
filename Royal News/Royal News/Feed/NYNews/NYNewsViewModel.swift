//
//  NYNewsViewModel.swift
//  Royal News
//
//  Created by Catalina on 7/31/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
class NYNewsViewModel {
    let tableView = UITableView(frame: .zero, style: .plain)
    let manager = XMLManager()
    init() {
        configureTableView()
    }
    func configureTableView() {
        tableView.register(NewsTableViewCell.self, forCellReuseIdentifier: NewsTableViewCell.reuseID)
        tableView.tableFooterView = UIView()
    }
    func getNews(url: String, completionHandler: @escaping ((Result<[News], RNError>) -> Void)) {
        manager.parse(url: url) { result in
            completionHandler(result)
        }
    }
}
