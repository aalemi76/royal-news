//
//  FollowingViewController.swift
//  Royal News
//
//  Created by Catalina on 7/30/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
class FollowingViewController: UIViewController {
    weak var coordinator: FollowingCoordinator?
    let model = FollowingViewModel()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = GlobalSettings.shared().lightGray
        addTableView()
    }
    func configureNavigationBar() {
        guard let navController = navigationController as? RNNavigationController else {return}
        navController.setNavigationBarHidden(false, animated: true)
        navController.backIcon.isHidden = true
        navigationItem.title = "Following"
    }
    func addTableView() {
        let tableView = model.tableView
        tableView.delegate = self
        tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)])
    }
}
