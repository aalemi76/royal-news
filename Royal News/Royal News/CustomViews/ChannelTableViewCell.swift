//
//  ChannelTableViewCell.swift
//  Royal News
//
//  Created by Catalina on 7/31/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit

class ChannelTableViewCell: UITableViewCell {
    static let reuseID = "ChannelTableViewCell"
    let img: UIImageView = {
       let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.layer.cornerRadius = 5
        img.layer.masksToBounds = true
        img.widthAnchor.constraint(equalToConstant: 50).isActive = true
        img.heightAnchor.constraint(equalToConstant: 50).isActive = true
        return img
    }()
    let label: UILabel = {
        let label = UILabel()
        label.font = GlobalSettings.shared().boldSystemFont(size: 16)
        label.textAlignment = .left
        return label
    }()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configure()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func configure() {
        img.translatesAutoresizingMaskIntoConstraints = false
        addSubview(img)
        NSLayoutConstraint.activate([
            img.centerYAnchor.constraint(equalTo: centerYAnchor),
            img.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15)])
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)
        NSLayoutConstraint.activate([
            label.centerYAnchor.constraint(equalTo: img.centerYAnchor),
            label.leadingAnchor.constraint(equalTo: img.trailingAnchor, constant: 8)])
    }
    func confgiureCellItems(title: String, image: UIImage) {
        label.text = title
        img.image = image
    }
}
