//
//  NewsTableViewCell.swift
//  Royal News
//
//  Created by Catalina on 7/31/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
protocol NewsTableViewCellDelegate: class {
    func didTapLikeButton(cell: NewsTableViewCell)
}
class NewsTableViewCell: UITableViewCell {
    static let reuseID = "NewsTableViewCell"
    weak var delegate: NewsTableViewCellDelegate?
    let newsImage = UIImageView()
    let srcLabel = UILabel()
    let titleLabel = UILabel()
    let descriptionLabel = UILabel()
    let dateLabel = UILabel()
    let likeButton = UIButton(type: .system)
    var downloadTask: URLSessionDownloadTask?
    var isFavorite: Bool = false {
        willSet {
            changeLikeButton(state: newValue)
        }
    }
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, MMM d, yyyy"
        return formatter
    }()
    @objc func likeButtonTouchUpInside() {
        isFavorite = !isFavorite
        delegate?.didTapLikeButton(cell: self)
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = GlobalSettings.shared().lightGray
        configureImageView()
        configureTitle()
        configureSource()
        confgiureDesription()
        configureDate()
        configureButton()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func configure(forNews news: News) {
        titleLabel.text = news.title
        srcLabel.text = news.src
        descriptionLabel.text = news.itemDescription
        if let date = news.date {
            dateLabel.text = dateFormatter.string(from: date)
        }
        isFavorite = news.isFavorite
        guard let url = URL(string: news.imageURL) else { return }
        downloadTask = newsImage.loadImage(url)
    }
    func changeLikeButton(state: Bool) {
        if state {
            likeButton.setTitle(" Unfavorite", for: .normal)
            likeButton.setImage(UIImage(systemName: "heart.slash.fill"), for: .normal)
        } else {
            likeButton.setImage(UIImage(systemName: "heart"), for: .normal)
            likeButton.setTitle(" Favorite", for: .normal)
        }
    }
    func configureImageView() {
        newsImage.translatesAutoresizingMaskIntoConstraints = false
        addSubview(newsImage)
        NSLayoutConstraint.activate([
            newsImage.centerYAnchor.constraint(equalTo: centerYAnchor),
            newsImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            newsImage.widthAnchor.constraint(equalToConstant: 80),
            newsImage.heightAnchor.constraint(equalToConstant: 80)])
        newsImage.layer.cornerRadius = 40
        newsImage.layer.masksToBounds = true
        newsImage.layer.borderWidth = 2
        newsImage.layer.borderColor = GlobalSettings.shared().darkGray.cgColor
    }
    func configureTitle() {
        configure(label: titleLabel, font: GlobalSettings.shared().boldSystemFont(size: 15), color: .black)
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            titleLabel.leadingAnchor.constraint(equalTo: newsImage.trailingAnchor, constant: 8),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8)])
    }
    func configureSource() {
        configure(label: srcLabel, font: GlobalSettings.shared().systemFont(size: 15), color: GlobalSettings.shared().darkGray)
        NSLayoutConstraint.activate([
            srcLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 3),
            srcLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            srcLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor)])
    }
    func confgiureDesription() {
        configure(label: descriptionLabel, font: GlobalSettings.shared().systemFont(size: 12), color: GlobalSettings.shared().darkGray)
        descriptionLabel.numberOfLines = 3
        descriptionLabel.lineBreakMode = .byTruncatingTail
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: srcLabel.bottomAnchor, constant: 5),
            descriptionLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            descriptionLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor)])
    }
    func configureDate() {
        configure(label: dateLabel, font: GlobalSettings.shared().systemFont(size: 15), color: GlobalSettings.shared().mainColor)
        NSLayoutConstraint.activate([
            dateLabel.topAnchor.constraint(greaterThanOrEqualTo: descriptionLabel.bottomAnchor, constant: 10),
            dateLabel.topAnchor.constraint(greaterThanOrEqualTo: newsImage.bottomAnchor, constant: 10),
            dateLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            dateLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10)])
    }
    func configureButton() {
        likeButton.tintColor = GlobalSettings.shared().mainColor
        likeButton.setImage(UIImage(systemName: "heart"), for: .normal)
        likeButton.setTitle(" Favorite", for: .normal)
        likeButton.setTitleColor(GlobalSettings.shared().mainColor, for: .normal)
        likeButton.titleLabel?.font = GlobalSettings.shared().systemFont(size: 15)
        likeButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(likeButton)
        NSLayoutConstraint.activate([
            likeButton.centerYAnchor.constraint(equalTo: dateLabel.centerYAnchor),
            likeButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15)])
        likeButton.addTarget(self, action: #selector(likeButtonTouchUpInside), for: .touchUpInside)
    }
    func configure(label: UILabel, font: UIFont, color: UIColor) {
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = font
        label.textColor = color
        label.textAlignment = .left
        addSubview(label)
    }
}
